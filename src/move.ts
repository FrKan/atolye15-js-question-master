type File = {
  id: string;
  name: string;
};

type Folder = {
  id: string;
  name: string;
  files: File[];
};

const NOT_FOUND_INDEX = -1;

export default function move(list: Folder[], source: string, destination: string): Folder[] {
  let sourceFileIndex = NOT_FOUND_INDEX;
  let sourceFolderIndex = NOT_FOUND_INDEX;
  let destinationFolderIndex = NOT_FOUND_INDEX;

  list.some((folder: Folder, index: number) => {
    // Check if destination folder exists
    if (folder.id === destination) {
      destinationFolderIndex = index;
    }

    // Check if source file exists
    const matchedFileIndex: number = folder.files.findIndex((file: File) => file.id === source);
    if (matchedFileIndex !== NOT_FOUND_INDEX) {
      sourceFileIndex = matchedFileIndex;
      sourceFolderIndex = index;
    }

    // Break loop if requirements satisfied
    return destinationFolderIndex !== NOT_FOUND_INDEX && sourceFolderIndex !== NOT_FOUND_INDEX;
  });

  if (sourceFolderIndex === NOT_FOUND_INDEX) {
    throw new Error('You cannot move a folder');
  }

  if (destinationFolderIndex === NOT_FOUND_INDEX) {
    throw new Error('You cannot specify a file as the destination');
  }

  // Deep clone to protect initial state of input
  const clonedList: Folder[] = <Folder[]>JSON.parse(JSON.stringify(list)); // Basic deep clone approach.

  // Move file
  const movingFile: File = clonedList[sourceFolderIndex].files.splice(sourceFileIndex, 1)[0];
  clonedList[destinationFolderIndex].files.push(movingFile);

  return clonedList;
}
